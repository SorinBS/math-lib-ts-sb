# math-lib-ts-sb

math-lib-ts-sb is a typescript library for formating numbers.

## Installation

Use the package manager to install math-lib-ts-sb.

```bash
npm install math-lib-ts-sb
```

## Usage

```typescript 
import * as mathLibSb from 'math-lib-ts-sb';

# returns '10.2'
mathLibSb.getFloatNumberWithDot('10,2')

# returns '10,2'
mathLibSb.getFloatNumberWithComma('10.2')

# returns '10.26'
mathLibSb.truncateNumberWithoutRounding('10.263')

# returns '10.263'
mathLibSb.truncateNumberWithoutRounding('10.2634', 3)
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://www.npmjs.com/package/math-lib-ts-sb)