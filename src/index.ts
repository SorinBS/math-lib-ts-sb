export function getFloatNumberWithDot(value: string) {
    if (value && value.includes(',')) {
        return parseFloat(value.replace(',', '.'));
    } else {
        return parseFloat(value);
    }
}

export function getFloatNumberWithComma(value: string) {
    if (value && value.includes('.')) {
        return value.replace('.', ',');
    } else {
        return value;
    }
}

export function truncateNumberWithoutRounding(value: string, numberOfDecimals = 2) {
    const numberPartials = value.split('.');

    if (numberPartials.length > 1) {
        const decimals = numberPartials[1].substring(0, numberOfDecimals);

        return [numberPartials[0], decimals].join('.');
    } else {
        return value;
    }
}